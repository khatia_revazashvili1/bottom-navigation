package com.example.myapplication.Fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.myapplication.R

class SearchFragment: Fragment(R.layout.fragment_search){


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.textView).text =
            SearchFragmentArgs.fromBundle(requireArguments()).amount.toString()
    }

}